UNIT = {
	0=> "" ,
	1=> "one ",
    2=> "two ",
    3=> "three ",
    4=> "four ",
    5=> "five ",
    6=> "six ",
    7=> "seven ",
    8=> "eight ",
    9=> "nine "
}
HANGCHUC={
	0=> "",
	1=> "ten ",
    2=> "twenty ",
    3=> "thirty ",
    4=> "forty ",
    5=> "fifty ",
    6=> "sixty ",
    7=> "seventy ",
    8=> "eighty ",
    9=> "ninety "
}
TEENS ={
	10=> "ten",
    11=> "eleven ",
    12=> "twelve ",
    13=> "thirteen ",
    14=> "fourteen ",
    15=> "fifteen ",
    16=> "sixteen ",
    17=> "seventeen ",
    18=> "eighteen ",
    19=> "nineteen "
}
SCALE = {
	1=>"hundred ",
    2=>"thousand ",
    3=>"million ",
    4=>"billion ",
    5=>"trillion ",
    6=>"quadrillion ",
    7=>"quintillion ",
    8=>"Sextillion ",
    9=>"Septillion ",
    10=>"Octillion ",
    11=>"Nonillion ",
    12=>"Decillion ",
    13=>"Undecillion ",
    14=>"Duodecillion ",
    15=>"Tredecillion ",
    16=>"Quattuordecillion ",
    17=>"Quindecillion ",
    18=>"Sexdecillion ",
    19=>"Septendecillion ",
    20=>"Octodecillion ",
    21=>"Novemdecillion ",
    22=>"Vigintillion " #10^63
}

def spell(n)
	spell_n = ""
	if n==0
		return "zero"
	elsif n.to_s.length==3
		a = n.to_s[0].to_i
		b = n.to_s[1].to_i
		c = n.to_s[2].to_i
		t = n - a*100
		if t <= 19 && t >=10
			return spell_n + UNIT[a]+SCALE[1]+TEENS[t]
		else 
			
			return spell_n + UNIT[a]+SCALE[1]+" and "+HANGCHUC[b] + "-" +UNIT[c]
		end
	elsif n.to_s.length==2
		if n <= 19 && n >=10
			return spell_n + TEENS[n]
		else 
			a = n.to_s[0].to_i
			b = n.to_s[1].to_i
			return spell_n + HANGCHUC[a] +"-" + UNIT[b]
		end
	elsif n.to_s.length==1
		return spell_n + UNIT[n]
	else recursive_spell(n,spell_n)
	end
end

def recursive_spell(n,spell_n)
	num_n = n.to_s.length
	if n == 0 
		return spell_n
	end
	if num_n <= 3 then
		return spell_n + spell(n)
	end
	if num_n%3 ==0 then
		scale_n = num_n/3
	else 
		scale_n = num_n/3 + 1
	end
	t = 10**((scale_n-1)*3)
	temp_n = (n - n%t)/t #get num of SCALE
	
	if temp_n.to_s.length==3
		a = temp_n.to_s[0].to_i
		b = temp_n.to_s[1].to_i
		c = temp_n.to_s[2].to_i
		t_n = temp_n - a*100
		if t_n <= 19 && t_n >=10
			spell_n = spell_n + UNIT[a] + SCALE[1] +TEENS[t_n] + SCALE[scale_n]
		else 
			spell_n = spell_n + UNIT[a] + SCALE[1] + HANGCHUC[b] + UNIT[c] + SCALE[scale_n]
		end
	elsif  temp_n.to_s.length==2
		if temp_n<=19 && temp_n>=10
			spell_n = spell_n + TEENS[temp_n] + SCALE[scale_n]
		else	
			a = temp_n.to_s[0].to_i
			b = temp_n.to_s[1].to_i
			spell_n = spell_n + HANGCHUC[a]  +UNIT[b] + SCALE[scale_n]
		end
	else 
		spell_n = spell_n + UNIT[temp_n] + SCALE[scale_n]
	end
	return recursive_spell(n-temp_n*t,spell_n)
	
end
printf "Input Number: "
n = gets.chomp.to_i
puts spell n