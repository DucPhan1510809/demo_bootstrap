class Article < ApplicationRecord
	belongs_to :user
	has_many :comments, dependent: :destroy 
	 validates :title, presence: true,
					length: { minimum: 5 }

	# validate :validate_title


	# def validate_title
	# 	if !self.title.present?
	# 		errors.add(title: "Wrong!!!!!!")
	# 	end
	# end
end
